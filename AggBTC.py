from pymongo import MongoClient, errors
import os
import json
from datetime import datetime
import websocket
import rel
import redis
from candle_maker import NumberBasedCandleMaker, PercentBasedCandleMaker

r = redis.Redis(host='localhost', port=6379, db=0, charset="utf-8", decode_responses=True)
make_candle = PercentBasedCandleMaker(0.01)


def write_db(data, selected_key, db= "AggCandle", col="btc2percent"):
    client = MongoClient("mongodb://user:pass@192.168.11.48:27017/?authSource=admin&authMechanism=SCRAM-SHA-256") 
    db = client[db]
    pred_collection = db[col]
    if isinstance(data, dict):
        try:
            print(datetime.now())
            result = pred_collection.insert_many([data], ordered=False, bypass_document_validation=True)
        except errors.BulkWriteError as e:
            print("duplicated is ignored....!")
    elif isinstance(data, list):
        try:
            # print('data to write:', data, type(data))
            print(datetime.now())
            result = pred_collection.insert_many([data], ordered=False, bypass_document_validation=True)
        except errors.BulkWriteError as e:
            print("duplicated is ignored....!")
    
    r.delete(selected_key)


def on_open(ws):
    print("connection opened")

def on_close(ws, close_status_code, close_msg):
    print("close_status_code: ", close_status_code)
    print("close_msg: ", close_msg)
    print("closed connection")

def on_error(ws, error):
    print(error)

def on_message(ws, message):
# {
#   "e": "aggTrade",  // Event type
#   "E": 123456789,   // Event time
#   "s": "BNBBTC",    // Symbol
#   "a": 12345,       // Aggregate trade ID
#   "p": "0.001",     // Price
#   "q": "100",       // Quantity
#   "f": 100,         // First tradزمثشقe ID
#   "l": 105,         // Last trade ID
#   "T": 123456785,   // Trade time
#   "m": true,        // Is the buyer the market maker?
#   "M": true         // Ignore
#     }
    data = json.loads(message)
    # print(data)
    r.set('id_' + str(data['a']), message)
    r.expire('id_' + str(data['a']), 60*60*24)
    
    result = make_candle.add_one_trade(data)
    
    if result is not None and result['symbol']:
        try:
            selected_key = 'id_' + str(data['a'])
            write_db(result, selected_key)
        except Exception as e:
            print('cant make candle', e)
    
def call_scoket():
    my_pid = os.getpid()
    pid_key = 'pid_' + str(int(datetime.now().replace(microsecond=0).timestamp()))
    r.set(pid_key , my_pid)

    BINANCE_SOCKET = "wss://fstream.binance.com/ws/btcusdt@aggTrade"
    # websocket.enableTrace(True)
    websocket._socket.setdefaulttimeout(60)
    ws = websocket.WebSocketApp(BINANCE_SOCKET,on_open=on_open, on_close=on_close, on_error=on_error, on_message=on_message)
    ws.run_forever(dispatcher=rel, reconnect=5) # Set dispatcher to automatic reconnection, 5 second reconnect delay if connection closed unexpectedly
    rel.signal(2, rel.abort)  # Keyboard Interrupt
    try:
        rel.dispatch()
    except BrokenPipeError:
        print('BrokenPipeError')



if __name__ == '__main__':
    
    while True:
        try:
            print('started call_scoket:', datetime.now())
            call_scoket()
        except Exception as e:
            print('error: ', e)
